from django.urls import reverse
from django.db import models
from django.db.models import Count
from django_jalali.db import models as jmodels
from gol_boote.settings import AUTH_USER_MODEL as User


class Group_news(models.Model):
    name_group_news = models.CharField(max_length=100, db_index=True)
    slug_group_news = models.SlugField(max_length=200, unique=True, null=True)

    def __str__(self):
        return self.name_group_news

    def get_absolute_url(self):
        return reverse('news:news_category', args=[self.slug_group_news])

    def get_count_news(self):
        salam = News.objects.filter(group_news=self.id).count()

        return salam


class News(models.Model):
    STATUS = (
        (0, "Draft"),
        (1, "Publish")
    )
    IMPORTANT = (
        (0, "NO"),
        (1, "YES")
    )
    group_news = models.ForeignKey(
        Group_news, on_delete=models.CASCADE, related_name='group_news')
    title_news = models.CharField(max_length=200, unique=True)
    slug_news = models.SlugField(max_length=200, unique=True)
    author_news = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name='author_news')
    important_news = models.IntegerField(choices=IMPORTANT, default=0)
    image = models.ImageField(upload_to='image_news/', blank=True, null=True)
    content_news = models.TextField()
    objects = jmodels.jManager()
    created_news = jmodels.jDateTimeField(auto_now_add=True)
    status_news = models.IntegerField(choices=STATUS, default=0)

    def get_absolute_url(self):
        return reverse('news:news_detail', args=[self.slug_news])

    def get_absolute_url_category(self):
        return reverse('news:news_detail', args=[self.slug_news])

    class Meta:
        ordering = ['-created_news']

    def __str__(self):
        return self.title_news


class Cause(models.Model):
    STATUS = (
        (0, "غیر فعال"),
        (1, "فعال")
    )

    title_cause = models.CharField(max_length=200, unique=True)
    slug_cause = models.SlugField(max_length=200, unique=True)
    image = models.ImageField(upload_to='image_cause/', blank=True, null=True)
    content_cause = models.TextField()
    objects = jmodels.jManager()
    created_cause = jmodels.jDateTimeField(auto_now_add=True)
    status_cause = models.IntegerField(choices=STATUS, default=0)
    goal_cause = models.PositiveIntegerField(null=True, blank=True)
    reised_cause = models.PositiveIntegerField(null=True, blank=True)

    class Meta:
        ordering = ['-created_cause']

    def get_absolute_url(self):
        return reverse('news:cause_detail', args=[self.slug_cause])

    def get_percent(self):
        return (self.reised_cause / self.goal_cause)*100

    def __str__(self):
        return self.title_cause

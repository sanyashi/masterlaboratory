from django.contrib import admin
from .models import News, Group_news, Cause
from django_jalali.admin.filters import JDateFieldListFilter
import django_jalali.admin as jadmin


class News_Admin(admin.ModelAdmin):
    prepopulated_fields = {'slug_news': ('title_news',)}

    list_display = ('title_news', 'important_news',
                    'status_news', 'created_news')
    list_filter = ("status_news",)
    search_fields = ['title_news']


class Group_Admin(admin.ModelAdmin):
    prepopulated_fields = {'slug_group_news': ('name_group_news',)}


class Cause_Admin(admin.ModelAdmin):
    prepopulated_fields = {'slug_cause': ('title_cause',)}
    list_display = ['title_cause', 'status_cause',
                    'goal_cause', 'reised_cause']
    list_editable = ['goal_cause', 'reised_cause', 'status_cause']
    list_filter = ["status_cause", ]


admin.site.register(Group_news, Group_Admin)

admin.site.register(News, News_Admin)

admin.site.register(Cause, Cause_Admin)

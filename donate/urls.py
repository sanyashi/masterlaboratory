
# from django.conf.urls import url, re_path, path
from . import views
from django.urls import path

app_name = 'donate'


urlpatterns = [
    path('', views.donate, name='root_donate'),

]

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.utils.safestring import mark_safe
import json
import os
from account.models import Responsible
from account.models import CustomUser as User
from .decorators_role import msool_login_required, mk_login_required, mj_login_required


@login_required(login_url='/account/login/')
def room(request, room_name):
    salam = str(request.user.img_user)
    salam = "/media/" + salam
    return render(request, 'chat/room.html', {
        'room_name_json': mark_safe(json.dumps(room_name)),
        'username': mark_safe(json.dumps(request.user.username)),
        'img_user': mark_safe(json.dumps(salam)),

    })


@login_required(login_url='/account/login/')
def index(request):
    salam = "/media/" + str(request.user.img_user)
    responsible = Responsible.objects.all()

    return render(request, 'chat/room.html', {
        'room_name_json': mark_safe(json.dumps(1)),
        'lname': request.user.last_name,
        'username': mark_safe(json.dumps(request.user.username)),
        'img_user': mark_safe(json.dumps(salam)),
        'responsible': responsible,

    })


@msool_login_required
def gold(request):
    salam = "/media/" + str(request.user.img_user)
    responsible = Responsible.objects.all()

    return render(request, 'chat/room.html', {
        'room_name_json': mark_safe(json.dumps(2)),
        'username': mark_safe(json.dumps(request.user.username)),
        'img_user': mark_safe(json.dumps(salam)),
        'responsible': responsible,

    })


@mk_login_required
def mk(request):
    salam = "/media/" + str(request.user.img_user)
    responsible = Responsible.objects.all()

    return render(request, 'chat/room.html', {
        'room_name_json': mark_safe(json.dumps(3)),
        'username': mark_safe(json.dumps(request.user.username)),
        'img_user': mark_safe(json.dumps(salam)),
        'responsible': responsible,

    })


@mj_login_required
def mj(request):
    salam = "/media/" + str(request.user.img_user)
    responsible = Responsible.objects.all()

    return render(request, 'chat/room.html', {
        'room_name_json': mark_safe(json.dumps(4)),
        'username': mark_safe(json.dumps(request.user.username)),
        'img_user': mark_safe(json.dumps(salam)),
        'responsible': responsible,

    })

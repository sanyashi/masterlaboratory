from django.contrib import admin

from .models import Message


class Message_Admin(admin.ModelAdmin):
    list_display = ['author', 'content',
                    'message_grp']
    list_filter = ["message_grp"]
    search_fields = ['content']


admin.site.register(Message, Message_Admin)

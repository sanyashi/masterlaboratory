from django.contrib.auth.decorators import login_required
from django.dispatch import receiver
from constance.signals import config_updated
from constance import config
from django.shortcuts import render, redirect
from .forms import ImageForm
from account.models import CustomUser as User
from django.contrib import auth
from news.models import News, Cause
from .models import Responsible
import json
import urllib
from django.conf import settings

from datetime import timedelta
from online_users import models as counterm
from django.contrib.auth.decorators import login_required, user_passes_test


def is_recruiter(self):
    if self.confirm_role == 1:
        return True
    else:
        return False


rec_login_required = user_passes_test(
    lambda u: True if is_recruiter(u) else False, login_url='/')


def recruiter_login_required(view_func):
    decorated_view_func = login_required(
        rec_login_required(view_func), login_url='/')
    return decorated_view_func


def login(request):
    if request.method == 'POST':
        user = auth.authenticate(
            username=request.POST['username'], password=request.POST['password'])
        if user is not None:
            auth.login(request, user)
            return redirect('/')
            recaptcha_response = request.POST.get('g-recaptcha-response')
            url = 'https://www.google.com/recaptcha/api/siteverify'
            values = {
                'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                'response': recaptcha_response
            }
            data = urllib.parse.urlencode(values).encode()
            req = urllib.request.Request(url, data=data)
            response = urllib.request.urlopen(req)
            result = json.loads(response.read().decode())
            if result['success']:
                auth.login(request, user)
                return redirect('/')
            else:
                return render(request, 'login.html', {'error': 'Invalid reCAPTCHA. Please try again'})
        else:
            return render(request, 'login.html', {'error': 1})
    else:
        return render(request, 'login.html')


@recruiter_login_required
def dashboard(request):
    form = ImageForm()
    return render(request, 'dashboard.html', {'form': form})


@receiver(config_updated)
def constance_updated(sender, key, old_value, new_value, **kwargs):
    print(sender, key, old_value, new_value)


def index(request):
    
    responsible = Responsible.objects.order_by("?")[:3]
    cause = Cause.objects.order_by("?")[:3]
    news = News.objects.order_by("?")[:3]
    user_activity_objects = counterm.OnlineUserActivity.get_user_activities()
    number_of_active_users = user_activity_objects.count() + 5

    try:
        return render(request, 'index.html', {'cause': cause, 'news': news, 'config': config, 'counter': number_of_active_users, 'responsible': responsible})
    except ValueError:
        salam = {'MADAD_JOO': '20', 'MADAD_KAR': '10'}
        return render(request, 'index.html', {'cause': cause, 'news': news, 'config': salam, 'responsible': responsible})


def error(request):
    return render(request, 'error.html')


def about(request):
    responsible = Responsible.objects.order_by("?")[:9]
    user_activity_objects = counterm.OnlineUserActivity.get_user_activities()
    number_of_active_users = user_activity_objects.count() + 5

    try:
        return render(request, 'about.html', {'config': config, 'responsible': responsible, 'counter': number_of_active_users})
    except ValueError:
        salam = {'MADAD_JOO': '20', 'MADAD_KAR': '10'}
        return render(request, 'about.html', {'config': salam, 'responsible': responsible, 'counter': number_of_active_users})


def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        return redirect('/account/login')

    else:
        return render(request, 'login.html')


def signup(request):
    if request.method == 'POST':
        form = ImageForm(request.POST, request.FILES)
        if form.is_valid():
            if form.cleaned_data['password'] == form.cleaned_data['confirm']:
                try:
                    user = User.objects.get(
                        username=form.cleaned_data['username'])
                    form = ImageForm()
                    return render(request, 'signup.html', {'error': 'این نام کاربری در حال حاضر موجود است ، نام دیگری انتخاب کنید', 'form': form})
                except User.DoesNotExist:
                    user = User.objects.create_user(
                        form.cleaned_data['username'], img_user=form.cleaned_data['img_user'], email=form.cleaned_data['email'], password=form.cleaned_data['password'], address_user=form.cleaned_data['address_user'], first_name=form.cleaned_data['first_name'], last_name=form.cleaned_data['last_name'], phone_user=form.cleaned_data['phone_user'], comment_user=form.cleaned_data['comment_user'], role_user=form.cleaned_data['role_user'], jens_user=form.cleaned_data['jens_user'])
                    auth.login(request, user)
                    return redirect('/')
            else:
                form = ImageForm()
                return render(request, 'signup.html', {'error': 'رمز و تکرار آن یکسان نیست', 'form': form})

    else:

        form = ImageForm()

        return render(request, 'signup.html', {'form': form})

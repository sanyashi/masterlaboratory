% Options for packages loaded elsewhere
\PassOptionsToPackage{unicode}{hyperref}
\PassOptionsToPackage{hyphens}{url}
%
\documentclass[12pt
]{article}
\usepackage{lmodern}
\usepackage[a4paper, total={6in, 10in}]{geometry}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
  \usepackage{textcomp} % provide euro and other symbols
\else % if luatex or xetex
  \usepackage{unicode-math}
  \defaultfontfeatures{Scale=MatchLowercase}
  \defaultfontfeatures[\rmfamily]{Ligatures=TeX,Scale=1}
\fi
% Use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
\IfFileExists{microtype.sty}{% use microtype if available
  \usepackage[]{microtype}
  \UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\makeatletter
\@ifundefined{KOMAClassName}{% if non-KOMA class
  \IfFileExists{parskip.sty}{%
    \usepackage{parskip}
  }{% else
    \setlength{\parindent}{0pt}
    \setlength{\parskip}{6pt plus 2pt minus 1pt}}
}{% if KOMA class
  \KOMAoptions{parskip=half}}
\makeatother
\usepackage{xcolor}
\IfFileExists{xurl.sty}{\usepackage{xurl}}{} % add URL line breaks if available
\IfFileExists{bookmark.sty}{\usepackage{bookmark}}{\usepackage{hyperref}}
\hypersetup{
  hidelinks,
  pdfcreator={LaTeX via pandoc}}
\urlstyle{same} % disable monospaced font for URLs
\setlength{\emergencystretch}{3em} % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{-\maxdimen} % remove section numbering

\hypersetup{
	pdftitle={Dimostrazioni e quesiti per primo parziale di Teoria dei Segnali},
	pdfauthor={Leonardo Toccafondi},
	hidelinks,
	pdfcreator={LaTeX via pandoc}}

\title{Dimostrazioni e quesiti per primo parziale di Teoria dei Segnali}
\author{}
\date{}

\begin{document}
	\maketitle

\hypertarget{quesiti}{%
\subsection{QUESITI}\label{quesiti}}

\begin{enumerate}
\def\labelenumi{\arabic{enumi}.}

\item
	Un segnale deterministico a tempo continuo s(t) periodico ha Energia e Potenza finite e/o infinite ?
	
\item
Si definisca la Potenza istantanea di un segnale deterministico a
tempo continuo $s(t)$

\item 
Si definisca la potenza media di un segnale deterministico a tempo continuo $s(t)$

\item
Calcolare Energia e Potenza media del segnale a tempo continuo
\(x(t) = Ae ^{-t} u(t)\).

\item
Calcolare l'energia del segnale x(t) = sinc(t).

\item
Calcolare Energia e Potenza media di un segnale a tempo continuo
sinusoidale di ampiezza 2 e di periodo 2 secondi.
\item
Calcolare Energia e Potenza media di un segnale a tempo continuo
sinusoidale di ampiezza A e di periodo T.

\item
Calcolare Energia e Potenza media di un segnale a tempo continuo
costante

\item 
Sotto quali condizioni un segnale deterministico a tempo continuo s(t) ha Potenza media finita?

\item
Giustificare la seguente affermazione: ``Lo spettro di ampiezza di un
segnale tempo-continuo aperiodico reale è una funzione pari''.

\item
Giustificare la seguente affermazione: ``Lo spettro di fase di un
	segnale tempo-continuo aperiodico reale è una funzione dispari''.


\item
Calcolare la quantità
\(\displaystyle \int_{- \infty}^{\infty} sinc(t) dt\)
	
\item 
Tra onda quadra e onda triangolare, quale dei due segnali ha coefficienti della serie di Fourier con modulo che
va a zero più velocemente al crescere di n?

\item
Tra la rampa e l'onda triangolare, quale dei due segnali ha
coefficienti della serie di Fourier con modulo che va a zero più
velocemente al crescere di n, e perché?

\item
Quali sono le proprietà della serie di Fourier di un segnale periodico
pari

\item
Quali sono le proprietà della serie di Fourier di un segnale periodico
dispari

\item 
Se il segnale periodico $x(t)$ è reale e pari, allora la serie di Fourier si semplifica in modo tale che ...


\item 
Se il segnale periodico $x(t)$ è reale e dispari, allora la serie di Fourier si semplifica in modo tale che ...


\item
Elencare le Condizioni di Dirichlet per la convergenza della serie di
Fourier

\item 
Elencare le Condizioni di Dirichlet per la convergenza della trasformata di Fourier



\item 
Se il segnale aperiodico $x(t)$ è pari, allora la sua trasformata di Fourier è	

\item 
Se il segnale aperiodico $x(t)$ è dispari, allora la sua trasformata di Fourier è

\item 
Se il segnale aperiodico $x(t)$ è reale e pari, allora la sua trasformata di Fourier è

\item
Se il segnale aperiodico $x(t)$ è reale e dispari, allora la sua
trasformata di Fourier è\ldots{}

\item 
Se la trasformata di Fourier di $x(t)$ è $X(f)$, allora la trasformata di $x(\alpha t)$ con $|\alpha|>1$ risulta modificata in modo
che ...

\item 
Se la trasformata di Fourier di $x(t)$ è $X(f)$, allora la trasformata di $x(\alpha t)$ con $|\alpha|>1$ risulta modificata in modo che ...